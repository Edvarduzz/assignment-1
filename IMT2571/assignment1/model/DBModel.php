<?php
include_once("Model.php");
//include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
            
			try
			{
			$this->db = new PDO('mysql:host=localhost;dbname=cool_books', 'root', '');
			// Set the PDO error mode to exception
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			echo "Connected successfully. <br>";
			}
			
			catch(PDOException $e)
			{
			echo " <br>Connection failed: " . $e->getMessage();
			echo "<br>";
      		}
    }}
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		$booklist = array();
		$stmt=$this->db->prepare("SELECT * FROM books");
		$stmt->execute();
		while ($row=$stmt->fetchObject()){
			$booklist[]=new Book($row->title, $row->author, $row->description, $row->id);
			}
        return $booklist;
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		//if (is_numeric $id)
			//return null;
		$book = null;
		$stmt=$this->db->prepare("SELECT * FROM books Where id=:id");
		$stmt->execute([":id"=>$id]);
		$row=$stmt->fetchObject();
		if ($row)
			$book=new Book($row->title, $row->author, $row->description, $row->id);
		return $book;
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
		if(empty($book->author || $book->title))
			throw new Exeption("Title and author cannot be empty");
		$stmt=$this->db->prepare('INSERT INTO books (title, author, description)values(:title,:author,:description)');
		$stmt->execute([":title"=>$book->title,
						":author"=>$book->author,
						":description"=>$book->description]);
		$book->id=$this->db->lastInsertId();
		
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
		$stmt=$this->db->prepare("UPDATE books SET title=:title, author=:author, description=:description where id=:id");
		$stmt->execute([":title"=>$book->title,
						":author"=>$book->author,
						":description"=>$book->description,
						":id"=>$book->id]);
				
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
		if (!is_numeric ($id))
			throw new Exeption("No book with that id",1);
			
		$stmt=$this->db->prepare("DELETE FROM books WHERE id=:id");
		$stmt->bindValue(':id',$id);
		$stmt->execute();
		
		
    }
	
}

?>